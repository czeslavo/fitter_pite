from setuptools import setup

setup(
    name='fitter',
    author='Grzegorz Burzynski',
    version='0.0.0-dev',
    license='BSD',
    packages=[
        'fitter',
    ],
    install_requires=[
        'pytest',
        'numpy',
        'click',
        'matplotlib',
        'scipy'
    ],
    entry_points={
        'console_scripts': [
            'fit_and_test = scripts.fit_and_test:fit_and_test'
           ]
    }
)

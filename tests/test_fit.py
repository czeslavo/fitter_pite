from fitter.fitter import Fitter
from fitter.generator import *
import matplotlib.pyplot as plt

def test_fitter():
    gen = Meter(15, multiply_noise=0.2, x_noise=1, abs_noise=1)

    x_values = gen.get_x_values()
    y_values = gen.get_y_values()

    fitter = Fitter()
    fitter.fit(x_values, y_values)

    y_result = []
    for x in x_values:
        y_result.append(fitter.function(x))

    plt.plot(x_values, y_values)
    plt.plot(x_values, y_result)
    plt.show()

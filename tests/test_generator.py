import fitter.generator as gen
import math
import numpy as np
import matplotlib.pyplot as plt
from fitter.generator import Meter


def test_pm10_return_x():
    meter = Meter(5)

    x_values = meter.get_x_values()

    assert x_values

def test_pm10_return_y():
    meter = Meter(4)

    y_values = meter.get_y_values()

    assert y_values

def test_plotting():
    meter = Meter(5, abs_noise=0, x_noise=60 * 8)

    y_values = meter.get_y_values()
    x_values = meter.get_x_values()

    plt.plot(x_values, y_values)
    plt.show()
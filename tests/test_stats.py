from scipy.stats.mstats import chisquare
from fitter.fitter import Fitter
from fitter.generator import *


def test_chisquare():
    gen = Meter(max_sample=10, multiply_noise=2, abs_noise=1, x_noise=0)
    xdata = gen.get_x_values()
    ydata = gen.get_y_values()

    fitter = Fitter()
    fitter.fit(xdata, ydata)

    print(chisquare(ydata, fitter.ydata_fit(xdata)))


from scipy.stats.mstats import chisquare


class Chisquare:
    """Chi square test for testing the independence."""

    def __init__(self, observed, expected):
        """
        :param observed: data observed
        :param expected: data expected (the same size as observed)
        """
        result = chisquare(observed, expected)
        self.pvalue = result.pvalue
        self.statistic = result.statistic

    def __str__(self):
        """Returns string representation of test results."""

        return "statistic = %2.2f; p = %2.2f" % (self.statistic, self.pvalue)

    def result(self, tolerance):
        """
        :param tolerance: Max tolerance for test result.
        :return: string representation of passing, or not passing the test
        """
        if 1 - self.pvalue < tolerance:
            return "hypothesis passed"
        else:
            return "hypothesis rejected"

from scipy.optimize import curve_fit
import math
import numpy as np


class Fitter:
    """ Fits function a*cos(x+b)+c to given data."""

    def __init__(self):
        self.function = None
        self.function_string = ""

    def fit(self, xdata, ydata):
        """Fits and stores the fitted function and its string representation."""

        def function(x, alpha, gamma, phi):
            return alpha * np.cos(x + gamma) + phi

        params = curve_fit(function, xdata, ydata)[0]
        self.function = lambda x: params[0] * math.cos(x + params[1]) + params[2]
        self.function_string = "%2.2f * cos(x + %2.2f) + %2.2f" % tuple(params)

    def ydata_fit(self, xdata):
        """Returns values of the fitted function for given xdata."""

        ydata_fit = []
        for x in xdata:
            ydata_fit.append(self.function(x))
        return ydata_fit

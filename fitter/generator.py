import random
import datetime
import math
import numpy as np

def noiser(multiply_noise, abs_noise, x_noise):
    """ Decorator for class method. It adds noise to the output.
    :param multiply_noise: max value of noise by which the output will be multiplied
    :param abs_noise: max absolute noise which will be added to the output
    :param x_noise: max x offset which will be performed
    """

    def wrapper(f):
        def wrapped_f(self=None, x=0):
            if random.choice([True, False]):
                result = f(self, x + random.random() * x_noise) * (1 + random.random() * multiply_noise)
            else:
                result = f(self, x - random.random() * x_noise)  * (1 - random.random() * multiply_noise)

            if random.choice([True, False]):
                result += random.random() * abs_noise
            else:
                result -= random.random() * abs_noise

            if result <= 0:
                return 0
            else:
                return result

        return wrapped_f

    return wrapper


class Meter():
    """Fake meter which gives xdata and ydata on output."""

    def __init__(self, max_sample=1, multiply_noise=0, abs_noise=0, x_noise=0):
        self.max_sample = max_sample
        self.multiply_noise = multiply_noise
        self.abs_noise = abs_noise
        self.x_noise = x_noise

    def get_x_values(self):
        """Returns sample xvalues."""

        x_values = []
        for x in np.arange(0, self.max_sample, 0.05):
            x_values.append(x)

        return x_values

    def get_x_dates(self):
        start_date = datetime.datetime.now() - datetime.timedelta(weeks=50)
        dates = []
        for x in np.arange(0, self.max_sample, 0.05):
            dates.append(start_date + datetime.timedelta(minutes=x * 230))
        return dates

    def get_y_values(self):
        """Returns yvalues corresponding to xvalues."""

        y_values = []

        for x in np.arange(0, self.max_sample, 0.05):
            y_values.append(self.get_y_value(x))

        return y_values

    def get_y_value(self, x):
        """Returns single yvalue for given x."""

        @noiser(multiply_noise=self.multiply_noise, abs_noise=self.abs_noise, x_noise=self.x_noise)
        def y_value(self=self, x=x):
            return 60 * (math.cos(x) + 1.2)

        return y_value(self, x)



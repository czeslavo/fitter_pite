# USAGE
```sh
$ virtualenv env
$ source env/bin/activate
$ cd CLONED_PROJECT_DIR
$ pip install .
$ fit_and_test --help
```

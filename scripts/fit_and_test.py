import click
from fitter.fitter import Fitter
from fitter.generator import *
from fitter.stats import Chisquare
import matplotlib.pyplot as plt


@click.command()
@click.option("-bound", type=click.FLOAT, help="Bound of x values", default=6.28)
@click.option("-m", type=click.FLOAT, help="Multiply noise", default=0.0)
@click.option("-a", type=click.FLOAT, help="Absolute noise", default=0.0)
@click.option("-x", type=click.FLOAT, help="X noise", default=0)
@click.option("-tolerance", type=click.FLOAT, help="Tolerance for chi square test", default=0.05)
def fit_and_test(bound, m, a, x, tolerance):
    """Generates cosinus function with given noises.
    After it, fits the function to model a * cos(x + b) + c.
    Draws both - fitted and observed function and checks their similarity
    with chi squared test. """

    gen = Meter(max_sample=bound, multiply_noise=m, abs_noise=a, x_noise=x)
    xdata = gen.get_x_values()
    ydata = gen.get_y_values()

    fitter = Fitter()
    fitter.fit(xdata, ydata)
    fig = plt.gcf()
    fig.canvas.set_window_title('PM10 level in Cracow')

    mng = plt.get_current_fig_manager()
    mng.resize(*mng.window.maxsize())
    plt.plot(gen.get_x_dates(), ydata, 'ro')
    plt.plot(gen.get_x_dates(), fitter.ydata_fit(xdata))
    plt.ylabel("[ug/m^3]")
    chi = Chisquare(ydata, fitter.ydata_fit(xdata))

    title = str(chi) + " (" + chi.result(tolerance) + ")"
    title += "\nfor noises: m = %2.1f, a = %2.1f, x = %d" % (m, a, x)
    title += "\nf(x) = " + fitter.function_string
    plt.title(title)
    plt.show()



